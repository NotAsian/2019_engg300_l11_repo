#include "Arduino.h"
#include "Queue.h"
#include "motorCont300.h"

#define _NUMFLOORS 5
#define __F1 45
#define __F2 47
#define __F3 49
#define __F4 51
#define __F5 53

#define __EMERGANCYPIN 18

#define DEBUG

Queue _Queue = Queue(_NUMFLOORS);
//motor300 obj;
motorCont300 motor0;
int currentFloor;

void setup(){
  


  Serial2.begin(9600); 
  currentFloor = 0;

  pinMode(12, OUTPUT);
  motor0.attach();
  attachInterrupt(0, doEncoderA, CHANGE); //encoder ON PIN 2
  attachInterrupt(1, doEncoderB, CHANGE); //encoder ON PIN 3

  attachInterrupt(digitalPinToInterrupt(__EMERGANCYPIN), EMERGENCY, CHANGE);

  int x = 10;
  Serial2.print(x);

  Serial.begin(9600);
  
  motor0.setState(0); //home
}


int flor = 0;

void loop(){


  if (Serial2.available()){
    int comm = Serial2.read();
    if(comm >0 &&comm <= 5){
      _Queue.requestFloor(comm);
    }else if(comm == 6){
      DOORS();
    }else if(comm == 7){
      EMERGENCY();
    }
  }

  if (Serial.available()) {
    String line = Serial.readString();
    int x = (int)line[0]-48;
    int y = (int)line[2]-48;
    currentFloor = x;
    
    Serial.print("Command ");
    Serial.print(x);
    Serial.print(":");
    Serial.println(y);
    _Queue.requestFloor(y);
    _Queue.currentFloor = currentFloor;
    y = 0;
  }

  if(!motor0.motorMoving()) {
    flor = _Queue.getMove(); //uncomment if issues with setting inside if statement
    if(flor != 0) //if elevator has to move
    {  
      motor0.setState(0); //set state to moving =
      Serial.print("move");
      Serial.println(flor);
    }
  } else {
    motor0.move(flor); //MMMMMMMMMOOOOOOOOOVVVVVVVVVVVVEEEEEEEEEEEEEE
  }
  /*
  flor = _Queue.getMove();
	//motor set state if new floor here
	if (flor != 0)
   {
    motor0.setState(0);
   }
  while(motor0.motorMoving())
  {
	motor0.move(flor);
  }
  */
  
}

// io serial event
void serialEvent2(){
  
}

void DOORS(){
  
}

void EMERGENCY(){
  Serial.println("woo");
  motor0.halt();
}

void doEncoderA()
{
  motor0.encoder1.goA();
}


void doEncoderB()
{
 motor0.encoder1.goB();
}
