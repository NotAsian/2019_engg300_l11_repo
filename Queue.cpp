/*
 * This header is part of the code for an elevator project for ENGG2/300 at MQ
 * 
 * Developed by the team L11_C10
 */

#include "Arduino.h"
#include "Queue.h"

Queue::Queue(int floors)
{
    numFloors = floors;
    floorRequested = new bool[numFloors];
    for (int i = 0; i < numFloors; i++)
    {
        floorRequested[i] = false;
    }
    currentFloor = 0;
    up = false;
}

bool Queue::requestFloor(int pfloor)
{
    // sanity check that the request is for a valid floor
    if (pfloor > numFloors ||pfloor<= 0)
    {
        return false;
    }
    else
    {
        floorRequested[pfloor-1] = true;
        return true;
    }
}

bool Queue::resetFloor(int pfloor)
{
    // sanity check that the request is for a valid floor
    if (pfloor >= numFloors ||pfloor< 0)
    {
        return false;
    }
    else
    {
        floorRequested[pfloor] = false;
        return true;
    }
} 



int Queue::getMove(){
  int i = currentFloor;
  bool switched = false;
  resetFloor(currentFloor);
  while(i>=0 && i<numFloors){
    if(floorRequested[i]){
      return(i-currentFloor);
    }
    if(up){
      i++;
    }else{
      i--;
    }
    if(!switched && (i < 0 || i >= numFloors)){
      up = !up;
      i = currentFloor;
      switched = !switched;
    }
  }
  // if nopflooris found, defaults to current direction as down
  up = false;
  // Serial.println("Nopfloorrequested");
  return 0;
}


// this function should only be used for debugging
void Queue::printFloors()
{
  for (int i = 0; i < numFloors; i++)
  {
    Serial.print(i);
    Serial.print(":");
    Serial.print(floorRequested[i]);
    Serial.print(" ");
  }
  Serial.println();
}
