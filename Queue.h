/*
 * This header is part of the code for an elevator project for ENGG2/300 at MQ
 * 
 * Developed by the team L11_C10
 */

#ifndef QUEUE
#define QUEUE

#include "Arduino.h"

class Queue
{
    public:
        // attributes
        int numFloors;
        bool *floorRequested;
        int currentFloor;
        bool up;  // current direction the elevator is moving
       
        // constructor
        Queue(int floors);
        
        // methods
        bool requestFloor(int pfloor);
        bool resetFloor(int pfloor);
        int getMove();

        // debugging methods
        void printFloors();
};

#endif
